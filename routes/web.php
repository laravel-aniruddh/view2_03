<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// simple route
Route::get('/home',function (){
    return "Home Page";
});

route::get("/Home", function(){
    return view('home');
});

//Route Parameters

Route::get('userid/{u_id}',function($id){
    return view('user',['id'=>$id]);
});
Route::get('user/{u_id}/name/{u_name}', function($uid,$uname){
    return view('useridname', ['uid'=>$uid, 'uname'=>$uname]);
});

//Optional Parameters

Route::get('student/{name?}', function($name=null){
    return view('student',['name'=>$name]);
});

Route::get('name/{name?}',function($name='Enter Parameter Name'){
    return view('name',['name'=>$name]);
});

// Regular Expression 

Route::get('product/{p_name}', function($pname){
    return view('product',['pname'=>$pname]);
})->where('p_name','[A-Za-z]+');

Route::get('manager/{id}/{name}', function($id,$name){
    return view('manager',['id'=>$id , 'name'=>$name]);
})->where(['id' => '[0-9]+', 'name' => '[a-z]+']);

// Regular Expression helper method

Route::get('emplyee/{id}/{name}', function($id,$name){
    return view('myemplyee',['id'=>$id , 'name'=>$name]);
})->whereNumber('id')->whereAlpha('name');

// Redirect Routes with view
Route::view('login','mylogin');
Route::view('register','myregister');
Route::Redirect('login','register');

Route::Redirect('krishna','aniruddh',301);
Route::permanentRedirect('mahadev','anu');

// Fallback Routes

Route::Fallback(function(){
    return view('error404');
});